echo -e "${BBlue}##### Executing pre-commit hooks #####${Color_Off}"

if [ $(id -u) -eq 0 ]
then
  # Work around markdown lint installation failure in Gitlab CI environment
  gem install mdl
  patch -p2 venv/lib/python3.*/site-packages/pre_commit/languages/ruby.py -i .gitlab.pre-commit.patch -N --quiet -r- || echo "Done"
fi

pre-commit run --all-files

echo ""
