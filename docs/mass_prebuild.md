Mass prebuild
=============
Mass rebuild for build tooling prototypes

--- ---

Use case
--------

- As a build tool developer I need to verify that a change I prepared doesn't
  beak dozen of packages
- As an upstream developer, I want to get early feedback on a feature I'm
  implementing
- As a package maintainer, I want to assess the risk of performing a major
  upgrade

Note:
The second use case derives from the first

-- --

- Changes done to a package may break dependent packages
- Hard to assess the risk on making a change

Note:
Having the possibility to massively rebuild dependent package would decrease the risk
These UC are build tooling centric, but may actually apply to a wider range of applications: libraries, daemons.

--- ---

Existing solutions
------------------

- [Jan K mass rebuild](https://git.jankratochvil.net/?p=massrebuild.git;a=tree)
- Jeff Law scripts using Jenkins
- Make use of COPR (script from Tom Stellard)
- Make use of Koji (script from Florian Weimer)

Note:
Any link available for Jeff Law scripts ?

-- --

 - Reverse deps calculated on the fly
 - Prepare the build environment with dedicated packages
 - Custom packages to be built

Note:
Preliminary analysis of the available script showed common denominator
Modification of the list by the developer should be possible

--- ---

Proposal
--------

- Backend/frontend implementation
- Package the solution in order to make it available to fedora users

Note:
 - Add support for multiple infrastructures
 - Provide a common interface
 - Global availability useful for less critical packages (libraries) that provide interfaces

-- --

- Written in python
- Interface shall be generic to allow migration to another language
- Suggestions are welcome

Note:
 - No file extension, so that it can be replaced silently

--- ---

Comments ?
----------
